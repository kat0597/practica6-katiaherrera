package programa;


import java.util.Scanner;

import clases.GestorReservas;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		int op = 0;
		String nombre = "";
		String dni = "";
		String apellido = "";
		double precioMenu = 0;
		int numMesa = 0;
		String fecha = "";
		int mes = 0;
		
		
		GestorReservas reservas = new GestorReservas();
		
		reservas.altaHospedado("123456a", "Katia");
		reservas.altaHospedado("123456b", "Angel");
		reservas.altaHospedado("123456c", "Larisa");
		reservas.altaHospedado("1234d", "Patch");
		
		reservas.altaReservaMesa("Katia", "Herrera", 50, 1, "2019-02-25");
		reservas.altaReservaMesa("Angel", "Perez", 50, 2, "2019-03-07");
		reservas.altaReservaMesa("Larisa", "Istrate", 25, 5, "2019-02-15");
		reservas.altaReservaMesa("Patch", "Cipriano", 25, 4, "2019-02-05");
		

		do {
			// Creamos un menu para que el usuario pueda escoger la opcion deseada
			System.out.println("Escoge una de las opciones");
			System.out.println("1 - Dar de alta a 3 hospedados");
			System.out.println("2 - Buscar una reserva");
			System.out.println("3 - Eliminar una reserva");
			System.out.println("4 - Reservar 3 mesas");
			System.out.println("5 - Buscar una reserva de una mesa");
			System.out.println("6 - Eliminar una reserva");
			System.out.println("7 - Listar mesas por mes");
			System.out.println("0 - Salir");

			// Damos el valor de la opcion a nuestra variable
			op = input.nextInt();
			
			
			switch (op) {
			
			case 1:
				input.nextLine();
				
				System.out.println("Inserte un nombre");
				nombre = input.nextLine();
				System.out.println("Inserte un dni");
				dni = input.nextLine();
				
				reservas.altaHospedado(dni, nombre);
				
				System.out.println("Inserte un nombre");
				nombre = input.nextLine();
				System.out.println("Inserte un dni");
				dni = input.nextLine();
				
				reservas.altaHospedado(dni, nombre);
				
				System.out.println("Inserte un nombre");
				nombre = input.nextLine();
				System.out.println("Inserte un dni");
				dni = input.nextLine();
				
				reservas.altaHospedado(dni, nombre);
				
				reservas.listarReservasH();
				
				
				break;
				
			case 2:
				input.nextLine();
				System.out.println("Inserte el dni de la reserva para buscarlo");
				dni = input.nextLine();
				
				System.out.println(reservas.buscarReservaH(dni));
				break;
				
			case 3:
				
				input.nextLine();
				System.out.println("Inserte el dni de la reserva para eliminarlo");
				dni = input.nextLine();
				
				reservas.eliminarReservaH(dni);
				reservas.listarReservasH();
				break;
				
				
			case 4:
				input.nextLine();
				
				System.out.println("Introduzca un nombre");
				nombre = input.nextLine();
				System.out.println("Introduzca un apellido");
				apellido = input.nextLine();
				System.out.println("Introduzca un precio");
				precioMenu = input.nextDouble();
				System.out.println("Introduzca el numero de la mesa");
				numMesa = input.nextInt();
				reservas.altaReservaMesa(nombre, apellido, precioMenu, numMesa, fecha);
				
				System.out.println("Introduzca un nombre");
				nombre = input.nextLine();
				System.out.println("Introduzca un apellido");
				apellido = input.nextLine();
				System.out.println("Introduzca un precio");
				precioMenu = input.nextDouble();
				System.out.println("Introduzca el numero de la mesa");
				numMesa = input.nextInt();
				reservas.altaReservaMesa(nombre, apellido, precioMenu, numMesa, fecha);
				
				System.out.println("Introduzca un nombre");
				nombre = input.nextLine();
				System.out.println("Introduzca un apellido");
				apellido = input.nextLine();
				System.out.println("Introduzca un precio");
				precioMenu = input.nextDouble();
				System.out.println("Introduzca el numero de la mesa");
				numMesa = input.nextInt();
				reservas.altaReservaMesa(nombre, apellido, precioMenu, numMesa, fecha);
				
				reservas.listarMesas();
				
				
				
				
				break;
				
			case 5:
				input.nextLine();
				
				System.out.println("Introduzca el nombre de la reserva que desea buscar");
				nombre = input.nextLine();
				System.out.println("Introduxca el apellido");
				apellido = input.nextLine();
				
				System.out.println(reservas.buscarMesa(nombre, apellido));
				
				break;
				
			case 6:
				input.nextLine();
				
				System.out.println("Introduzca el nombre de la reserva que desea eliminar");
				nombre = input.nextLine();
				System.out.println("Introduxca el apellido");
				apellido = input.nextLine();
				reservas.eliminarReserva(nombre, apellido);
				
				reservas.listarMesas();
				
				break;
				
			case 7:
				
				System.out.println("Introduzca el numero del mes para mostrar las reservas");
				mes = input.nextInt();
				
				reservas.listarReservasMes(mes);
			
			break;
			
			}
			
			
		} while (op != 0);

		input.close();

	}

}
