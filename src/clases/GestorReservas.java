package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorReservas {
	
	private ArrayList<ReservasHotel> listaReservasHotel;
	private ArrayList<ReservasRestaurante> listaReservasRest;
	
	public GestorReservas() {
		
		listaReservasHotel = new ArrayList<ReservasHotel>();
		listaReservasRest = new ArrayList<ReservasRestaurante>();
	}
	
	//METODOS CON LA CLASE 1 - - - - - - - - - - - - - - - - - - - 
	
	public boolean existeReservaHotel(String dni) {
		for (ReservasHotel reservaH : listaReservasHotel) {
			if (reservaH != null && reservaH.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	
	public void altaHospedado(String dni, String nombreReservaH) {
		if((!existeReservaHotel(dni))) {
			ReservasHotel nuevaReservaH = new ReservasHotel(dni, nombreReservaH);
			nuevaReservaH.setFechaEntrada(LocalDate.now());
			listaReservasHotel.add(nuevaReservaH);
		} else {
			System.out.println("La reserva ya ha sido realizada con anterioridad");
		}
	}
	
	public void listarReservasH() {
		for(ReservasHotel reservaH : listaReservasHotel) {
			if (reservaH != null) {
				System.out.println(reservaH);
			}
		}
	}
	
	public ReservasHotel buscarReservaH(String dni) {
		for (ReservasHotel reservasHotel : listaReservasHotel) {
			if(reservasHotel != null && reservasHotel.getDni().equals(dni)) {
				return reservasHotel;
			}
		}
		return null;
	}
	
	
	public void eliminarReservaH(String dni) {
		Iterator<ReservasHotel> iteratorReservaH = listaReservasHotel.iterator();
		
		while (iteratorReservaH.hasNext()) {
			ReservasHotel reservasHotel = iteratorReservaH.next();
			if(reservasHotel.getDni().equals(dni)) {
				iteratorReservaH.remove();
			}
		}
	}
	
	//METODOS CONN LA CLASE 2  - - - - - - - - - - - - - - - - - - - 
	
	public boolean existeReservaMesa(String nombre, String apellidos) {
		for (ReservasRestaurante mesa : listaReservasRest) {
			if (mesa != null && mesa.getNombreReservaH().equals(nombre)) {
				if (mesa != null && mesa.getApellidoH().contentEquals(apellidos))
				return true;
			}
		}
		return false;
	}
	
	public void altaReservaMesa(String nombreReservaR, String apellidoR, double precioMenu, int numeroMesa, String fechaReserva) {
		
		if((!existeReservaMesa(nombreReservaR,apellidoR))) {
		ReservasRestaurante mesa = new ReservasRestaurante(nombreReservaR,apellidoR,precioMenu, numeroMesa);
		mesa.setHoraReserva(LocalDate.parse(fechaReserva));
		listaReservasRest.add(mesa);
		
		}else {
		System.out.println("Ya tiene reserva");
		}
	}
	
	public void eliminarReserva(String nombre, String apellido) {
		Iterator<ReservasRestaurante> iteradorReservas = listaReservasRest.iterator();

		while (iteradorReservas.hasNext()) {
			ReservasRestaurante mesa = iteradorReservas.next();
			if (mesa.getNombreReservaH().equals(nombre)) {
				if(mesa.getApellidoH().equals(apellido)) {
				iteradorReservas.remove();
				}
			}
		}
	}
	
	public ReservasRestaurante buscarMesa(String nombre, String apellido) {
		for (ReservasRestaurante mesa : listaReservasRest) {
			if(mesa != null && mesa.getNombreReservaH().equals(nombre)) {
				if(mesa != null && mesa.getApellidoH().equals(apellido)) {
					return mesa;
				}
			}
		}
		return null;
	}
	
	public void listarMesas() {
		for(ReservasRestaurante mesa : listaReservasRest) {
			if (mesa != null) {
				System.out.println(mesa);
			}
		}
	}
	
	
	public void listarReservasMes(int mes) {
		// TODO Auto-generated method stub
		for(ReservasRestaurante mesa : listaReservasRest) {
			if (mesa.getHoraReserva().getMonthValue() == mes) {
				System.out.println(mesa);
			}
		}
		
	}
	
	
	//METODOS ESPECIALES - - - - - - - - - - - - - - - - - - - - - 
	





	
}
