package clases;

import java.time.LocalDate;

public class ReservasHotel {
	
	private String nombreReservaH;
	private String dni;
	private LocalDate fechaEntrada;
	
	//Constructor
	public ReservasHotel(String dni, String nombreH) {
		this.dni=dni;
		this.nombreReservaH=nombreH;
	}

	public  ReservasHotel(String nombreReserva) {
		this.nombreReservaH = nombreReserva;
	}

	public String getNombreReservaH() {
		return nombreReservaH;
	}

	public void setNombreReservaH(String nombreReservaH) {
		this.nombreReservaH = nombreReservaH;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDate getFechaEntrada() {
		return fechaEntrada;
	}

	public void setFechaEntrada(LocalDate fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}
	
	

	@Override
	public String toString() {
		return "ReservasHotel [nombreReservaH=" + nombreReservaH + ", dni=" + dni + ", fechaEntrada=" + fechaEntrada + "]";
	}
	

	
	
}
