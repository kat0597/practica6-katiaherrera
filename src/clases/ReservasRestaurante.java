package clases;

import java.time.LocalDate;

public class ReservasRestaurante {

	private String nombreReservaR;
	private String apellidoR;
	private double precioMenu;
	private int numMesa;
	private LocalDate horaReserva;
	
	
	//Constructor
	public ReservasRestaurante(String nombreReservaR, String apellidoR, double precioMenu,int numMesa) {
		this.nombreReservaR = nombreReservaR;
		this.apellidoR = apellidoR;
		this.numMesa = numMesa;
		this.precioMenu = precioMenu;
	}
	
	
	//GGSS
	
	public String getNombreReservaH() {
		return nombreReservaR;
	}
	public void setNombreReservaH(String nombreReservaH) {
		this.nombreReservaR = nombreReservaH;
	}
	public String getApellidoH() {
		return apellidoR;
	}
	public void setApellidoH(String apellidoH) {
		this.apellidoR = apellidoH;
	}
	public double getPrecioMenu() {
		return precioMenu;
	}
	public void setPrecioMenu(double precioMenu) {
		this.precioMenu = precioMenu;
	}
	public int getNumMesa() {
		return numMesa;
	}
	public void setNumMesa(int numMesa) {
		this.numMesa = numMesa;
	}
	public LocalDate getHoraReserva() {
		return horaReserva;
	}
	public void setHoraReserva(LocalDate horaReserva) {
		this.horaReserva = horaReserva;
	}
	
	
	
	@Override
	public String toString() {
		return "ReservasRestaurante [nombreReservaH=" + nombreReservaR + ", apellidoH=" + apellidoR + ", precioMenu="
				+ precioMenu + ", numMesa=" + numMesa + ", horaReserva=" + horaReserva + "]";
	}
	
	
	

}
